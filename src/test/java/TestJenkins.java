import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class TestJenkins {
	
	@Test
	public void testInteger() {
		assertTrue(1==1);
		assertFalse(1==2);
	}
	
	@Test
	public void testString() {
		assertTrue("a".equals("a"));
		assertFalse("a".equals("b"));
	}
	
	@Test
	public void testObject() {
		class Counter {
			private int number;
			Counter(int number) {
				this.number = number;
			}
			public void count() {
				this.number++;
			}
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + number;
				return result;
			}
			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				Counter other = (Counter) obj;
				if (number != other.number)
					return false;
				return true;
			}
		}

		assertTrue(new Counter(1).equals(new Counter(1)));
		assertFalse(new Counter(1).equals(new Counter(2)));
		Counter a = new Counter(1);
		a.count();
		assertTrue(a.equals(new Counter(2)));
	}
	
}
