public class Main {

	private static final String HELLO = "Hello, Jenkins";

	public static void main(String[] args) {
		Guesser guess = new Guesser();
		if(guess.guess(Math.random())) {
			System.out.println(HELLO + " you are a very special Jenkins");
		} else {
			System.out.println(HELLO + " you are welcome");
		}
	}
}
