public class Guesser {

	private final double keyword;
	
	public Guesser() {
		this.keyword = Math.random();
	}
	
	public boolean guess(final double educatedGuess) {
		return keyword == educatedGuess;
	}
}
